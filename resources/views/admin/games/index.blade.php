@extends('layouts.app')

@section('content')
<div class="container col-md-10 col-md-offset-2">
    <div class="card mt-5">

        <div class="card-header d-flex justify-content-between">

            <h5 class="float-left">All Games</h5>
            <h5 class="float-right"> <a href="{{ route('admin.games.create') }}" class="btn btn-success">Add a new Game</a> </h5>
            <a href="{{ route('admin.games.index') }}" class="btn btn-info">All Games</a>

        </div>

        <div class="content">

            @include('components.message')

            <div class="table-responsive">
                <table class="table">

                    <thead>
                        <tr>

                            <th>name</th>
                            <th>Image</th>
                            <th>categories</th>
                            <th>quantiy</th>
                            <th>Software House</th>
                            <th>Inventory</th>
                            <th>Actions</th>

                        </tr>
                    </thead>

                    <tbody>
                        @foreach($games as $game)

                            <tr>
                                <td>{{ $game->name }}</td>

                                <td>  <img src="{{Storage::url($game->image)}}" class="img-fluid" style="width: 150px;height:100%">   </td>

                                <td>
                                    @foreach ($game->categories as $category)
                                        <h5> {{ $category->name }}</h5>
                                    @endforeach
                                </td>

                                <td>  {{$game->quantity}} </a> </td>

                                <td>{{ $game->softwareHouse }}</td>

                                <td>
                                    <div class="d-flex flex-column justify-content-center">
                                        <a href="{{ route('admin.games.show',$game) }}" class="btn btn-info">Manage Inventory</a>
                                    </div>
                                </td>

                                <td>

                                    <form  style="display: inline-block" method="POST" action=" {{ route('admin.games.destroy',$game) }} ">
                                        @csrf
                                        @method('delete')

                                        <button type="submit"  class="">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </form>

                                    <a href=" {{ route('admin.games.edit',$game) }}"> <i class="fas fa-pencil-alt"></i> </a>


                                </td>

                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
@endsection
