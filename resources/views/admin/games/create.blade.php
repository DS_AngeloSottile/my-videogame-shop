@extends('layouts.app')

@section('content')
 <div class="container col-md-10 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header ">
                <h5 class="float-left">Create a new post</h5>
                <a href="{{ route('admin.games.index') }}" class="btn btn-info">All Games</a>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @include('components.message')

            <div class="card-body mt-2">
                <form method="post" action="{{ route('admin.games.store') }}" enctype="multipart/form-data">
                    @csrf


                    <div class="form-group">
                        <label for="name" class="col-lg-12 control-label">name</label>
                        <div class="col-lg-12">
                            <input type="text" class="form-control" id="name" placeholder="name" name="name" value="{{old('name') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="quantity" class="col-lg-12 control-label">quantity</label>
                        <div class="col-lg-12">
                            <input type="number" class="form-control" id="quantity" placeholder="quantity" name="quantity" value="{{old('quantity') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="softwareHouse" class="col-lg-12 control-label">Software House</label>
                        <div class="col-lg-12">
                            <input type="text" class="form-control" id="softwareHouse" placeholder="softwareHouse" name="softwareHouse" value="{{old('softwareHouse') }}">
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="categories" class="col-lg-12 control-label">Categories</label>
                        <div class="col-lg-12">
                            <select class="form-control" id="category" name="categories[]" multiple>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="file" name="image" class="form-control">
                    </div>

                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
