@extends('layouts.app')

@section('content')
<div class="container col-md-10 col-md-offset-2">
    <div class="card mt-5">

        <div class="card-header d-flex justify-content-between">

            <a href="{{ route('admin.games.index') }}" class="btn btn-info">All Games</a>
            <a href="{{ route('admin.games.show',$game) }}" class="btn btn-info">Manage Inventory</a>

        </div>

        <div class="content">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @include('components.message')

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>

                            <th>id</th>
                            <th>name</th>
                            <th>Image</th>
                            <th>categories</th>
                            <th>Software House</th>
                            <th>Stores linked</th>

                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>{{ $game->name }}</td>

                            <td>  <img src="{{Storage::url($game->image)}}" class="img-fluid" style="width: 150px;height:100%">   </td>

                            <td>
                                @foreach ($game->categories as $category)
                                    <h5> {{ $category->name }}</h5>
                                @endforeach
                            </td>

                            <td>
                                {{ $game->softwareHouse }}
                            </td>

                            <td>

                                <form action="{{ route('admin.games.connectStore',$game) }}" method="post">
                                    @csrf
                                    @method('patch')

                                    <div class="form-group">

                                        <label for="stores" class="col-lg-12 control-label">stores</label>

                                        <div class="col-lg-12">

                                            @foreach($stores as $store)
                                                <label for="stores" class="control-label">{{ $store->name }}</label>
                                                <input  id="genderM" name="store" type="radio" value="{{ $store->id }}">
                                            @endforeach

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="price" class="col-lg-12 control-label">price</label>
                                        <div class="col-lg-12">
                                            <input type="number" class="form-control" id="price" placeholder="price" name="price" value="{{old('price') }}">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-lg-10 col-lg-offset-2">
                                            <button type="submit" class="btn btn-primary">add to store</button>
                                        </div>
                                    </div>

                                </form>

                            </td>

                        </tr>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
@endsection
