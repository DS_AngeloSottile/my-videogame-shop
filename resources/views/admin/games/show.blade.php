@extends('layouts.app')

@section('content')
<div class="container col-md-10 col-md-offset-2">
    <div class="card mt-5">

        <div class="card-header d-flex justify-content-between">

            <div class="d-flex  justify-content-center align-items-center">
                @if($game->inventory->count() == $game->quantity )
                    <h5> Tutti i giochi di {{ $game->name }} sono statti aggiunti nei rispettivi stores</h5>
                @else

                    <h5> {{ $game->inventory->count() }} creati su   {{ $game->quantity }}</h5>
                    <a href="{{ route('admin.games.addToStock',$game) }}" class="btn btn-info">Add allo stock</a>

                @endif
            </div>

            <a href="{{ route('admin.games.index') }}" class="btn btn-info">All Games</a>

        </div>

        <div class="content">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @include('components.message')

            <div class="table-responsive">
                <table class="table">

                    <thead>
                        <tr>

                            <th>id</th>
                            <th>name</th>
                            <th>Image</th>
                            <th>quantity</th>
                            <th>categories</th>
                            <th>Software House</th>

                        </tr>
                    </thead>

                    <tbody>
                            <tr>
                                <td>{{ $game->id }}</td>
                                <td>{{ $game->name }}</td>

                                <td>  <img src="{{Storage::url($game->image)}}" class="img-fluid" style="width: 150px;height:100%">   </td>
                                <td>{{ $game->quantity }}</td>

                                <td>
                                    @foreach ($game->categories as $category)
                                        <h5> {{ $category->name }}</h5>
                                    @endforeach
                                </td>

                                <td>
                                    {{ $game->softwareHouse }}
                                </td>


                            </tr>
                    </tbody>

                </table>
            </div>
            <h1>IN STOCK</h1>
            @foreach($inventory as $product)

                <div class="table-responsive">
                    <table class="table">

                        <thead>
                            <tr>

                                <th>id</th>
                                <th>name</th>
                                <th>Image</th>
                                <th>categories</th>
                                <th>Software House</th>
                                <th>store</th>
                                <th>price</th>

                            </tr>
                        </thead>

                        <tbody>
                                <tr>
                                    <td>{{ $product->id }}</td>
                                    <td>{{ $product->game->name }}</td>

                                    <td>  <img src="{{Storage::url($product->game->image)}}" class="img-fluid" style="width: 150px;height:100%">   </td>

                                    <td>
                                        @foreach ($product->game->categories as $category)
                                            <h5> {{ $category->name }}</h5>
                                        @endforeach
                                    </td>

                                    <td> {{ $product->game->softwareHouse }}  </td>

                                    <td> {{ $product->store->name }}  </td>
                                    <td> {{ $product->price }}  </td>

                                </tr>
                        </tbody>

                    </table>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
