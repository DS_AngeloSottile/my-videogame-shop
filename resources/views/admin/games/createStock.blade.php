@extends('layouts.app')

@section('content')
 <div class="container col-md-10 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header ">
                <h5 class="float-left">Create a new Game </h5>
                <a href="{{ route('admin.games.index') }}" class="btn btn-info">All Games</a>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @include('components.message')

            <div class="card-body mt-2">
                <form method="post" action="{{ route('admin.games.store') }}" enctype="multipart/form-data">
                    @csrf



                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
