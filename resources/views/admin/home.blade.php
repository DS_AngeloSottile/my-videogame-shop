@extends('layouts.app')

@section('content')
  <div class="container">
        <div class="row banner">

            <div class="col-md-12 mt-5">

                <div class="list-group">

                    <div class="list-group-item">
                        <div class="row-content">
                            <h5><i class="far fa-grin-tongue mb-3"></i> Manage Categories</h5>
                            <a href="{{ route('admin.category.index') }} " class="btn btn-info">All Categories</a>
                            <a href="{{ route('admin.category.create') }}" class="btn btn-success">Add a new category</a>
                        </div>
                    </div>

                    <div class="list-group-separator"></div>

                    <div class="list-group-separator"></div>

                        <div class="list-group-item">
                            <div class="row-content">
                                <h5><i class="fas fa-file-signature mb-3"></i> Manage Games</h5>
                                <a href="{{ route('admin.games.index') }}" class="btn btn-info">All Games</a>
                                <a href="{{ route('admin.games.create') }}" class="btn btn-success">Add a new Game</a>
                            </div>
                        </div>

                    <div class="list-group-separator"></div>

                        <div class="list-group-item">
                            <div class="row-content">
                                <h5><i class="fas fa-cogs mb-3"></i> Manage Stores</h5>
                                <a href="{{ route('admin.store.index') }} " class="btn btn-info">All Stores</a>
                                <a href=" {{ route('admin.store.create') }}" class="btn btn-success">Add a new store</a>
                            </div>
                        </div>

                    <div class="list-group-separator"></div>
                </div>

            </div>

        </div>
    </div>
@endsection
