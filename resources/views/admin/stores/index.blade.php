@extends('layouts.app')

@section('content')
<div class="container col-md-10 col-md-offset-2">
    <div class="card mt-5">

        <div class="card-header d-flex justify-content-between">

            <h5 class="float-left">All Games</h5>
            <h5 class="float-right"> <a href="{{ route('admin.store.create') }}" class="btn btn-success">Add a new Store</a> </h5>
            <a href="{{ route('admin.store.index') }}" class="btn btn-info">All Stores</a>

        </div>

        <div class="content">

            @include('components.message')

            <div class="table-responsive">
                <table class="table">

                    <thead>
                        <tr>

                            <th>name</th>
                            <th>icon</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($stores as $store)

                            <tr>
                                <td>{{ $store->name }}</td>

                                <td>  <img src="{{Storage::url($store->icon)}}" class="img-fluid" style="width: 150px;height:100%">   </td>

                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
@endsection
