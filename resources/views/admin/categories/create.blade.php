@extends('layouts.app')

@section('content')
 <div class="container col-md-10 col-md-offset-2">
        <div class="mt-5 card">
            <div class="card-header d-flex justify-content-between ">
                <h5 class="float-left">Create a new category</h5>
                <a href="{{ route('admin.category.index') }}" class="btn btn-info">All Categories</a>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @include('components.message')

            <div class="mt-2 card-body">
                <form method="post" action="{{ route('admin.category.store') }}">
                    @csrf

                    <div class="form-group">
                        <label for="name" class="col-lg-12 control-label">name</label>
                        <div class="col-lg-12">
                            <input type="text" class="form-control" id="name" placeholder="name" name="name" value="{{old('name') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
