@extends('layouts.app')

@section('content')
<div class="container col-md-10 col-md-offset-2">
    <div class="card mt-5">

        <div class="card-header d-flex justify-content-between">

            <h5 class="float-left">All Categories</h5>
            <h5 class="float-right"> <a href="{{ route('admin.category.create') }}" class="btn btn-success">Add category</a> </h5>
            <a href="{{ route('admin.category.index') }}" class="btn btn-info">All categories</a>

        </div>

        <div class="content">

            @include('components.message')

            <div class="table-responsive">
                <table class="table">

                    <thead>
                        <tr>

                            <th>name</th>

                        </tr>
                    </thead>

                    <tbody>
                        @foreach($categories as $category)

                            <tr>

                                <td>{{ $category->name }}</td>

                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
@endsection
