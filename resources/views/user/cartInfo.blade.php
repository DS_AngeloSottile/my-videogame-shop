@extends('layouts.app')

@section('content')
<div class="container col-md-10 col-md-offset-2">
    <div class="card mt-5">

        <div class="card-header d-flex justify-content-between">

            <h5 class="float-left">All Games in cart</h5>
            <a href="{{ route('user-cart.cartBuy') }}" class="btn btn-info">Buy all</a>

        </div>

        <div class="content">

            @include('components.message')

            <ol class="list-group list-group-numbered">
                @foreach ( $user->cart->inventories as $item)

                    <li class="list-group-item d-flex justify-content-between align-items-start">
                        <div class="ms-2 me-auto">
                            <div class="fw-bold"></div>

                            <div class="col mb-5">
                                <div class="card h-100">

                                    <img class="card-img-top" src="{{ Storage::url($item->game->image) }}" alt="..." />

                                    <div class="card-body p-4">
                                        <div class="text-center">
                                            <h5 class="fw-bolder">{{ $item->game->name }}</h5>
                                            <div class="d-flex-justify-content-center">
                                                <span> {{$item->price}} $</span>
                                                <h4> {{$item->store->name}} </h4>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </li>

                @endforeach
            </ol>

        </div>
    </div>
</div>
@endsection
