@extends('layouts.app')

@section('content')
<div class="container col-md-10 col-md-offset-2">
    <div class="mt-5 card">

        <div class="content">

            @include('components.message')

            <div class="table-responsive">
                <table class="table">

                    <thead>
                        <tr>

                            <th>name</th>
                            <th>Image</th>
                            <th>categories</th>
                            <th>store</th>
                            <th>order Date</th>

                        </tr>
                    </thead>

                    <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{ $order->inventory->game->name }}</td>

                                <td>  <img src="{{Storage::url($order->inventory->game->image)}}" class="img-fluid" style="width: 150px;height:100%">   </td>

                                <td>
                                    @foreach ($order->inventory->game->categories as $category)
                                        <h5> {{ $category->name }}</h5>
                                    @endforeach
                                </td>
                                <td>
                                    {{ $order->inventory->store->name }}
                                </td>
                                <td>
                                    {{ $order->created_at }}
                                </td>


                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
@endsection
