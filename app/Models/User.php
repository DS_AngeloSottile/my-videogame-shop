<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable =
    [
        'name',
        'email',
        'password',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasRole( $role)
     {
        return $this->role->code === $role;
    }


    public function role() //relazione 1 user a n Ruoli
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function cart() //relazione 1 user a 1 carello
    {
        return $this->hasOne('App\Models\Cart');
    }

    public function orders() //relazione 1 user a n ordini
    {
        return $this->hasMany('App\Models\Order','user_id','id');
    }
}
