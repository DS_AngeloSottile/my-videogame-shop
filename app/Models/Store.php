<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'name',
        'icon',
    ];



    public function inventories() //relazione n a n stores
    {
        return $this->hasMany('App\Models\Inventory','store_id','id');
    }
}
