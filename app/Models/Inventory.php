<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $fillable =
    [
        'game_id',
        'store_id',
        'price'
    ];

    protected $with =
    [
        'store',
        'game',
    ];
    public function store() //relazione n a n stores
    {
        return $this->belongsTo('App\Models\Store','store_id','id');
    }

    public function game() //relazione n a n stores
    {
        return $this->belongsTo('App\Models\Game','game_id','id');
    }

    public function carts() //relazione n a n carts
    {
        return $this->belongsToMany('App\Models\Inventory','carts_inventories','inventory_id','cart_id');
    }


    public function orders()
    {
        return $this->hasMany('App\Models\Order','inventory_id','id');
    }

}
