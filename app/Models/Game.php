<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'name',
        'image',
        'quantity',
        'softwareHouse',
    ];

    protected $with =
    [
        'categories'
    ];

    public function categories() //relazione n a n categories
    {
        return $this->belongsToMany('App\Models\Category','games_categories','game_id','category_id');
    }

    public function inventory() //relazione 1 game a n inventory
    {
        return $this->hasMany('App\Models\Inventory','game_id','id');
    }
}
