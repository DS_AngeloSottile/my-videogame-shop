<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'name',
    ];


    public function games() //relazione n gategory n games
    {
        return $this->belongsToMany('App\Models\Game','games_categories','category_id','game_id');
    }

}
