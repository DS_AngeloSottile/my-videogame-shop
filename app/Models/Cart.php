<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Cart extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'user_id',
        'slug',
    ];


    public function inventories() //relazione n carts n inventories
    {
        return $this->belongsToMany('App\Models\Inventory','carts_inventories','cart_id','inventory_id');
    }

    public function user() //relazione 1 cart 1 user
    {
        return $this->belongsTo('App\Models\User');
    }
}
