<?php

namespace App\Providers;

use App\Models\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
            view()->composer('*', function ($view)
            {
                if(Auth::check())
                {
                    $cart = Cart::where('user_id', Auth::user()->id)->withCount('inventories')->first();

                    $view->with('cart', $cart );
                }

            });

    }
}
