<?php

namespace App\Traits;

use App\Models\Game;
use App\Models\Image;

trait ImgManageTrait
{

    private function addImage( $file, Game $game)
    {
        $imageName =  time(). '.' . $file->getClientOriginalExtension();
        $file_path = 'game/' .$game->id. '/img';
        $uploaded_file =  $file->storeAs( $file_path,$imageName,'public');

        $game->update(
            [
                'image' => $uploaded_file,
            ]
        );
    }

    private function editImage( $file, Game $game)
    {
        if(file_exists( public_path('storage/'.$game->image) ) )
        {
            unlink(public_path('storage/'.$game->image));
        }


        $imageName =  time(). '.' . $file->getClientOriginalExtension();
        $file_path = 'game/' .$game->id. '/img';
        $uploaded_file =  $file->storeAs( $file_path,$imageName,'public');


        $game->update(
            [
                'image' => $uploaded_file,
            ]
        );
    }

}

?>
