<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\Inventory;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $inventory = Inventory::with(['store','game.categories'])->get();

        return view('home',compact('inventory'));
    }

    public function adminIndex()
    {
        return view('admin.home');
    }
}
