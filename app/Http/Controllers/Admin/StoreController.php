<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateStoreRequest;
use App\Models\Store;
use App\Traits\ImgManageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StoreController extends Controller
{
    use ImgManageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();

        return view('admin.stores.index',compact('stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.stores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStoreRequest $request)
    {
        try
        {
            DB::beginTransaction();

                $store = Store::create(
                    [
                        'name'  => $request->name,
                    ]
                );

                if($request->has('icon'))
                {
                    $imageName =  time(). '.' . $request->icon->getClientOriginalExtension();
                    $file_path = 'stores' .$store->id. '/img';
                    $uploaded_file =  $request->icon->storeAs( $file_path,$imageName,'public');

                    $store->update(
                        [
                            'icon' => $uploaded_file,
                        ]
                    );
                }

            DB::commit();

                session()->flash('success','Il nuovo store è stato aggiunto');
                return redirect()->route('admin.store.index');
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Non è stato possibile aggiungere il nuovo store'
            ];
            session()->flash('error',$errors);
            return redirect()->back();
        }

    }

}
