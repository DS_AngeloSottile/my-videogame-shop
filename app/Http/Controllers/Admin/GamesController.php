<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ConnectStoresRequest;
use App\Http\Requests\GameStoreRequest;
use App\Http\Requests\UpdateGameRequest;
use App\Models\Category;
use App\Models\Game;
use App\Models\Inventory;
use App\Models\Store;
use App\Traits\ImgManageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class GamesController extends Controller
{
    use ImgManageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $games = Game::with(['inventory','categories'])->orderBy('created_at','desc')->get();

        return view('admin.games.index',compact('games'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('admin.games.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GameStoreRequest $request)
    {
        try
        {
            DB::beginTransaction();

                $game = Game::create(
                    [
                        'name' => $request->name,
                        'quantity' => $request->quantity,
                        'softwareHouse' => $request->softwareHouse,
                    ]
                );

                $game->categories()->sync($request->categories);

                if($request->hasFile('image'))
                {
                    $this->addImage($request->image, $game);
                }

            DB::commit();
                session()->flash('success','Il nuovo gioco è stato aggiunto');
                return redirect()->route('admin.games.index');
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Non è stato possibile aggiungere il gioco'
            ];
            session()->flash('error',$errors);
            return redirect()->back();
        }
    }

    public function addToStock(Game $game)
    {
        $stores = Store::all();

        return view('admin.games.addToStock',compact('stores','game'));
    }

    public function connectStore(ConnectStoresRequest $request,Game $game)
    {
        try
        {
            DB::beginTransaction();

                if($game->inventory->count() < $game->quantity)
                {
                    $inventory =Inventory::create(
                        [
                            'price' => $request->price,
                        ]
                    );
                    $inventory->store()->associate($request->store);
                    $inventory->game()->associate($game->id);
                    $inventory->save();
                }
                else
                {
                    $errors =
                    [
                        'error_message' => 'Limite massimo raggiunto'
                    ];
                    session()->flash('error',$errors);
                    return redirect()->route('admin.games.show',$game);
                }


            DB::commit();
            session()->flash('success','Il gioco è stato connesso allo store');
            return redirect()->route('admin.games.show',$game);
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Errore nell\'inserimento dello store'
            ];
            session()->flash('error',$errors);
            return redirect()->route('admin.games.show',$game);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Games  $games
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {

        $inventory = Inventory::with(['store','game.categories'])->where('game_id',$game->id)->get();

        return view('admin.games.show',compact('game','inventory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Games  $games
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        if(!$game)
        {
            $errors =
            [
                'error_message' => 'Gioco non trovato'
            ];

            session()->flash('error',$errors);
            return redirect()->back();
        }
        else
        {
            $categories = Category::all();
            $selectedCategories = $game->categories->pluck('id')->toArray();
            return view('admin.games.edit',compact('game','categories','selectedCategories'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Games  $games
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGameRequest $request, Game $game)
    {
        try
        {
            DB::beginTransaction();

               $game->update(
                   [
                    'name'                  => $request->name,
                    'softwareHouse'         => $request->softwareHouse,
                   ]
                );

                if($request->has('image'))
                {
                    $this->editImage($request->image,$game);
                }

                if($request->has('categories'))
                {
                    $game->categories()->sync($request->categories);
                }

                if($request->has('quantity'))
                {
                    if( $request->quantity >count($game->inventory) )
                    {
                        $game->update(
                            [
                                'quantity'  => $request->quantity
                            ]
                        );
                    }
                    else
                    {
                        $errors =
                        [
                            'error_message' => 'Non puoi inserire una quantità minore rispetto a giochi nell\'inventario clicca su manage inventory per vedere quante copie sono in vendità'
                        ];
                        session()->flash('error',$errors);
                        return redirect()->route('admin.games.index');
                    }
                }

            DB::commit();
                session()->flash('success','Il gioco è stato modificato');
                return redirect()->route('admin.games.index');
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Errore nella modifica del gioco'
            ];
            session()->flash('error',$errors);
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Games  $games
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game)
    {
        if(file_exists( public_path('storage/'.$game->image) ) )
        {
            unlink(public_path('storage/'.$game->image));
        }

       // $game->delete(); softdelete
        session()->flash('success','Il gioco è stato eliminato');
        return redirect()->route('admin.games.index');
    }
}
