<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('admin.categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        try
        {
            DB::beginTransaction();

                $category = Category::create([
                    'name'  => $request->name,
                ]);

            DB::commit();

                session()->flash('success','La nuova categoria è stata aggiunta');
                return redirect()->route('admin.category.index');
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Non è stato possibile aggiungere la nuova categoria'
            ];
            session()->flash('error',$errors);
            return redirect()->back();
        }
    }

}
