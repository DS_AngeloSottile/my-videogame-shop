<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Inventory;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserCartController extends Controller
{
    public function addToCart(Inventory $inventory)
    {
        try
        {
            DB::beginTransaction();


                $user = Auth::user();

                $cart = $user->cart()->first();

                $cart->inventories()->attach($inventory->id);

            DB::commit();
                session()->flash('success','Il gioco è stato inserito al tuo carello');
                return redirect()->route('home');
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Errore nella connessione al database'
            ];
            session()->flash('error',$errors);
            return redirect()->back();
        }
    }

    public function cartView()
    {
        $user = Auth::user();

        $user->load("cart.inventories.game");

        return view("user.cartInfo",compact("user"));
    }

    public function cartBuy()
    {

        try
        {
            DB::beginTransaction();


                $user = Auth::user();

                $cartGames =  $user->cart->inventories()->get();

                foreach($cartGames as $inventory)
                {

                    $orderData =
                    [
                        'user_id'       => $user->id,
                        'inventory_id'  => $inventory->id,
                    ];

                    $order = Order::create($orderData);
                    $actualQuantity = $inventory->game->quantity;

                    $inventory->game->update(
                        [
                            'quantity'  => $actualQuantity-1,
                        ]
                    );

                    $user->cart->inventories()->detach($inventory->id);
                    $inventory->delete();

                }


            DB::commit();
                session()->flash('success','Hai acquistato il gioco');
                return redirect()->back();
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Errore nell\'acquisto del suo gioco',
            ];
            session()->flash('error',$errors);
            return redirect()->back();
        }

    }
}
