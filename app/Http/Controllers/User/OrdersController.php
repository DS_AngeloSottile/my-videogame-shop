<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    public function orders()
    {
        $user = Auth::user();

        $orders = $user->orders()->with(['inventory' => fn($q) => $q->withTrashed()])->get();
        return view('user.profile',compact('user','orders'));
    }
}
