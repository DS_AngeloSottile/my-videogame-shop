<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/admin', [App\Http\Controllers\HomeController::class, 'adminIndex'])->name('adminIndex');


Route::prefix('admin-games-managment')->name('admin.')->middleware(['admin'])->group( function ()
{
    Route::resource('games', App\Http\Controllers\Admin\GamesController::class);

    Route::get('/games/addToStock/{game}', [App\Http\Controllers\Admin\GamesController::class, 'addToStock'])->name('games.addToStock');
    Route::patch('/games/connectStore/{game}', [App\Http\Controllers\Admin\GamesController::class, 'connectStore'])->name('games.connectStore');
});

Route::prefix('user-cart')->name('user-cart.')->middleware(['auth'])->group( function ()
{
    Route::get('addToCart/{inventory}', [App\Http\Controllers\User\UserCartController::class,'addToCart'])->name('addToCart');
    Route::get('cartView', [App\Http\Controllers\User\UserCartController::class,'cartView'])->name('cartView');
    Route::get('cartBuy', [App\Http\Controllers\User\UserCartController::class,'cartBuy'])->name('cartBuy');
});

Route::prefix('user-profile')->name('user-profile.')->middleware(['auth'])->group( function ()
{
    Route::get('profile', [App\Http\Controllers\User\OrdersController::class,'orders'])->name('profile');

});


Route::prefix('admin-categories-managment')->name('admin.')->group( function ()
{
    Route::resource('category', App\Http\Controllers\Admin\CategoriesController::class)->except('edit','update','show','destroy')->middleware(['admin']);

});

Route::prefix('admin-stores-managment')->name('admin.')->group( function ()
{
    Route::resource('store', App\Http\Controllers\Admin\StoreController::class)->except('edit','update','show','destroy')->middleware(['admin']);
});
