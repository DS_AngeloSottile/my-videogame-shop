<?php

namespace Database\Seeders;

use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $now = Carbon::now();
        $stores =
        [
            ['code' => 'admin', 'created_at' => $now, 'updated_at' => $now],
            ['code' => 'user',  'created_at' => $now, 'updated_at' => $now],
        ];

        Role::insert($stores);
    }
}
