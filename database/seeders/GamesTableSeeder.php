<?php

namespace Database\Seeders;

use App\Models\Game;
use Illuminate\Database\Seeder;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ( $i=1; $i<=5; $i++ )
        {
            $games = [];


            $games =
            [
                'name'                      => 'Game' . $i,
                'quantity'                  => 10,
                'image'                     => 'game/'.$i.'/img/1638710999.jpg',
                'softwareHouse'             => 'Ubisoft',
            ];


            Game::create($games);
        }
    }
}
