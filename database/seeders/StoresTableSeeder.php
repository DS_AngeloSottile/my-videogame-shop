<?php

namespace Database\Seeders;

use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $now = Carbon::now();
        $stores =
        [
            ['name' => 'Steam','icon' => '', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Uplay','icon' => '',  'created_at' => $now, 'updated_at' => $now],
            ['name' => 'EpicGames','icon' => '',  'created_at' => $now, 'updated_at' => $now],
        ];

        Store::insert($stores);
    }
}
