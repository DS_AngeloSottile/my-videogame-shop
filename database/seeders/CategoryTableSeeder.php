<?php

namespace Database\Seeders;

use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $now = Carbon::now();
        $categories =
        [
            ['name' => 'Sparatutto', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Platform',  'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Avvventura',  'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Horror',  'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Azione',  'created_at' => $now, 'updated_at' => $now]
        ];


        Category::insert($categories);
    }
}
